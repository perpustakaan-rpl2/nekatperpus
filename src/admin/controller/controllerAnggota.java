/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.controller;
import admin.dao.daoAnggota;
import admin.dao.daoKelas;
import admin.dao.implementAnggota;
import admin.dao.implementKelas;
import admin.model.modelAnggota;
import admin.model.modelKelas;
import admin.model.modelRak;
import admin.model.tableModelAnggota;
import admin.view.frameAnggota;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Risma Septiani
 */
public class controllerAnggota {
    frameAnggota fAnggota;
    implementAnggota iAnggota;
    implementKelas iKelas;   
    List<modelAnggota> lAnggota;
    List<modelKelas> lKelas;
    
    public controllerAnggota(frameAnggota fAnggota){
        this.fAnggota = fAnggota;
        iAnggota = new daoAnggota();
        lAnggota = iAnggota.getAll();
    }
    
    //reset field
    public void reset(){
        fAnggota.getIDAnggota().setText("");
        fAnggota.getNamaAnggota().setText("");
        fAnggota.getJurusan().setText("");
        fAnggota.getAlamat().setText("");
        fAnggota.getNoTelp().setText("");
    }
    
    //menampilkan data ke dalam tabel
    public void isiTabel(){
        lAnggota = iAnggota.getAll();
        tableModelAnggota tmAnggota = new tableModelAnggota(lAnggota);
        fAnggota.getTabelAnggota().setModel(tmAnggota);
    }
    
    //menampilkan data yang dipilih dari tabel 
    public void isiField(int row){
        fAnggota.getIDAnggota().setText(lAnggota.get(row).getIdAnggota().toString());
        fAnggota.getNamaAnggota().setText(lAnggota.get(row).getNamaAnggota());
        fAnggota.getJurusan().setText(lAnggota.get(row).getJurusan());
        fAnggota.getAlamat().setText(lAnggota.get(row).getAlamat());
        fAnggota.getNoTelp().setText(lAnggota.get(row).getNoTelp());
    }
    // insert data 
    public void insert() {
        modelAnggota b = new modelAnggota();
        b.setNamaAnggota(fAnggota.getNamaAnggota().getText()); 
        b.setJurusan(fAnggota.getJurusan().getText());
        b.setAlamat(fAnggota.getAlamat().getText());
        b.setNoTelp(fAnggota.getNoTelp().getText());
        
        iAnggota.insert(b);
    }
    
    //update data
    public void update() {
        modelAnggota b = new modelAnggota();
        b.setNamaAnggota(fAnggota.getNamaAnggota().getText());
        b.setJurusan(fAnggota.getJurusan().getText());
        b.setAlamat(fAnggota.getAlamat().getText());
        b.setNoTelp(fAnggota.getNoTelp().getText());
        b.setIdAnggota(Integer.parseInt(fAnggota.getIDAnggota().getText()));
        
        iAnggota.update(b);
    }
    public void delete() {
        if(!fAnggota.getIDAnggota().getText().trim().isEmpty()) {
            int id = Integer.parseInt(fAnggota.getIDAnggota().getText());
            iAnggota.delete(id);
        } else {
            JOptionPane.showMessageDialog(fAnggota, "Pilih data yang akan di hapus!");
        }
    }
 
    public Object getKelas() {
        iKelas = new daoKelas();
        lKelas = iKelas.getAll();
        
        return iKelas;
    }
}
