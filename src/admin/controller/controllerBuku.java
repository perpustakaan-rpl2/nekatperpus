/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.controller;
import admin.dao.daoBuku;
import admin.dao.implementBuku;
import admin.koneksi.koneksi;
import admin.model.modelBuku;
import admin.model.modelPenerbit;
import admin.model.modelPengarang;
import admin.model.modelRak;
import admin.model.tableModelBuku;
import admin.view.frameBuku;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Hanan
 */
public class controllerBuku {
    
    frameBuku fBuku;
    implementBuku iBuku;
    List<modelBuku> lBuku;
    Connection con = koneksi.connection();
    
    
    public controllerBuku(frameBuku fBuku) {
        this.fBuku = fBuku;
        iBuku = new daoBuku();
        lBuku = iBuku.getAll();
    }
    
    // reset field
    public void reset() {
        fBuku.getIDBuku().setText("");
        fBuku.getPenerbit().setSelectedItem("");
        fBuku.getPengarang().setSelectedItem("");
        fBuku.getRak().setSelectedItem("");
        fBuku.getJdlBuku().setText("");
        fBuku.getThnTerbit().setText("");
        fBuku.getJmlBuku().setText("");
        fBuku.getTglPengadaan().setText("");
        fBuku.getBtnTambah().setEnabled(true);
        fBuku.getBtnUbah().setEnabled(false);
        fBuku.getBtnHapus().setEnabled(false);
    }
    // menampilkan data ke dalam tabel
    public void isiTabel() {
        lBuku = iBuku.getAll();
        tableModelBuku tmBuku = new tableModelBuku(lBuku);
        fBuku.getTabelBuku().setModel(tmBuku);
    }
    
    // menampilkan data yang dipilih dari tabel
    public void isiField(int row) {
        fBuku.getIDBuku().setText(lBuku.get(row).getId_buku().toString());
        fBuku.getJdlBuku().setText(lBuku.get(row).getJudul_buku());
        fBuku.getPenerbit().setSelectedItem(lBuku.get(row).getNama_penerbit());
        fBuku.getPengarang().setSelectedItem(lBuku.get(row).getNama_pengarang());
        fBuku.getRak().setSelectedItem(lBuku.get(row).getKategori_rak());
        fBuku.getThnTerbit().setText(lBuku.get(row).getTahun_terbit());
        fBuku.getJmlBuku().setText(lBuku.get(row).getJumlah_buku());
        fBuku.getTglPengadaan().setText(lBuku.get(row).getTanggal_pengadaan());
        
    }
    
    // insert data 
    public void insert() {
        modelBuku b = new modelBuku();
        b.setJudul_buku(fBuku.getJdlBuku().getText()); 
        b.setId_penerbit(Integer.parseInt(this.getId_Penerbit()));
        b.setId_pengarang(Integer.parseInt(this.getId_Pengarang()));
        b.setId_rak(Integer.parseInt(this.getId_Rak()));
        b.setTahun_terbit(fBuku.getThnTerbit().getText());
        b.setJumlah_buku(fBuku.getJmlBuku().getText());
        b.setTanggal_pengadaan(fBuku.getTglPengadaan().getText());
        
        iBuku.insert(b);
    }
    
    //Update data
    public void update() {
        modelBuku b = new modelBuku();
        b.setJudul_buku(fBuku.getJdlBuku().getText()); 
        b.setId_penerbit(Integer.parseInt(this.getId_Penerbit()));
        b.setId_pengarang(Integer.parseInt(this.getId_Pengarang()));
        b.setId_rak(Integer.parseInt(this.getId_Rak()));
        b.setTahun_terbit(fBuku.getThnTerbit().getText());
        b.setJumlah_buku(fBuku.getJmlBuku().getText());
        b.setTanggal_pengadaan(fBuku.getTglPengadaan().getText());
        b.setId_buku(Integer.parseInt(fBuku.getIDBuku().getText()));
        
        
        iBuku.update(b);
    }
    
    //delete data 
    public void delete() {
        if(!fBuku.getIDBuku().getText().trim().isEmpty()) {
            int id = Integer.parseInt(fBuku.getIDBuku().getText());
            iBuku.delete(id);
        } else {
            JOptionPane.showMessageDialog(fBuku, "Pilih data yang akan di hapus!");
        }
    }
    
    public void comboPenerbit() {
        fBuku.getPenerbit().addItem("");
        try {
            Statement stm = con.createStatement();
            ResultSet sql = stm.executeQuery("select * from penerbit");
            while(sql.next()) {
                fBuku.getPenerbit().addItem(sql.getString("nama_penerbit"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(modelPenerbit.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void comboPengarang() {
        fBuku.getPengarang().addItem("");
        try {
            Statement stm = con.createStatement();
            ResultSet sql = stm.executeQuery("select * from pengarang");
            while(sql.next()) {
                fBuku.getPengarang().addItem(sql.getString("nama_pengarang"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(modelPengarang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void comboRak() {
        fBuku.getRak().addItem("");
        try {
            Statement stm = con.createStatement();
            ResultSet sql = stm.executeQuery("select * from rak");
            while(sql.next()) {
                fBuku.getRak().addItem(sql.getString("kategori_rak"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(modelRak.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private String getId_Penerbit() {
        try {
            String penerbit = fBuku.getPenerbit().getSelectedItem().toString();
            Statement stm = con.createStatement();
            ResultSet sql = stm.executeQuery("select * from penerbit where nama_penerbit = '"+penerbit+"'");
            sql.next();
            
            return sql.getString("id_penerbit");
        } catch (SQLException ex) {
            Logger.getLogger(modelPenerbit.class.getName()).log(Level.SEVERE, null, ex);
            return "Gagal";
        }
    
    }
    
    private String getId_Pengarang() {
        try {
            String pengarang = fBuku.getPengarang().getSelectedItem().toString();
            Statement stm = con.createStatement();
            ResultSet sql = stm.executeQuery("select * from pengarang where nama_pengarang = '"+pengarang+"'");
            sql.next();
            
            return sql.getString("id_pengarang");
        } catch (SQLException ex) {
            Logger.getLogger(modelPengarang.class.getName()).log(Level.SEVERE, null, ex);
            return "Gagal";
        }
    
    }
    
    private String getId_Rak() {
        try {
            String rak = fBuku.getRak().getSelectedItem().toString();
            Statement stm = con.createStatement();
            ResultSet sql = stm.executeQuery("select * from rak where kategori_rak = '"+rak+"'");
            sql.next();
            
            return sql.getString("id_rak");
        } catch (SQLException ex) {
            Logger.getLogger(modelRak.class.getName()).log(Level.SEVERE, null, ex);
            return "Gagal";
        }
    
    }
    
    
}
