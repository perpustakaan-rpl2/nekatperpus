/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.controller;

import admin.dao.daoPenerbit;
import admin.dao.implementPenerbit;
import admin.model.modelPenerbit;
import admin.model.tableModelPenerbit;
import admin.view.framePenerbit;
import java.util.List;
import javax.swing.JOptionPane;


/**
 *
 * @author Ibang
 */
public class controllerPenerbit {
    
    framePenerbit fPenerbit;
    implementPenerbit iPenerbit;
    List<modelPenerbit> lPenerbit;
    
    public controllerPenerbit(framePenerbit fPenerbit) {
        this.fPenerbit = fPenerbit;
        iPenerbit = new daoPenerbit();
        lPenerbit = iPenerbit.getAll();
    }
    
    // reset field
    public void reset() {
        fPenerbit.getIDPenerbit().setText("");
        fPenerbit.getNamaPenerbit().setText("");
        fPenerbit.getAlamatPenerbit().setText("");
    }
    
    // menampilkan data ke dalam tabel
    public void isiTabel() {
        lPenerbit = iPenerbit.getAll();
        tableModelPenerbit tmPenerbit = new tableModelPenerbit(lPenerbit);
        fPenerbit.getTabelPenerit().setModel(tmPenerbit);
    }
    
    // menampilkan data yang dipilih dari tabel
    public void isiField(int row) {
        fPenerbit.getIDPenerbit().setText(lPenerbit.get(row).getIdPenerbit().toString());
        fPenerbit.getNamaPenerbit().setText(lPenerbit.get(row).getNamaPenerbit());
        fPenerbit.getAlamatPenerbit().setText(lPenerbit.get(row).getAlamat());
        
    }
    
    // insert data 
    public void insert() {
        modelPenerbit b = new modelPenerbit();
        b.setNamaPenerbit(fPenerbit.getNamaPenerbit().getText()); 
        b.setAlamat(fPenerbit.getAlamatPenerbit().getText());
        
        iPenerbit.insert(b);
    }
    
    //Update data
    public void update() {
        modelPenerbit b = new modelPenerbit();
        b.setNamaPenerbit(fPenerbit.getNamaPenerbit().getText());
        b.setAlamat(fPenerbit.getAlamatPenerbit().getText());
        b.setIdPenerbit(Integer.parseInt(fPenerbit.getIDPenerbit().getText()));
        
        iPenerbit.update(b);
    }
    
    //delete data 
    public void delete() {
        if(!fPenerbit.getIDPenerbit().getText().trim().isEmpty()) {
            int id = Integer.parseInt(fPenerbit.getIDPenerbit().getText());
            iPenerbit.delete(id);
        } else {
            JOptionPane.showMessageDialog(fPenerbit, "Pilih data yang akan di hapus!");
        }
    }
}
