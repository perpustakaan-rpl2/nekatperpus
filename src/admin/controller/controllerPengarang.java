/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.controller;

import admin.dao.daoPengarang;
import admin.dao.implementPengarang;
import admin.model.modelPengarang;
import admin.model.tableModelPengarang;
import admin.view.framePengarang;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author dalex
 */
public class controllerPengarang {
    
    framePengarang fPengarang;
    implementPengarang iPengarang;
    List<modelPengarang> lPengarang;
    
    public controllerPengarang(framePengarang fPengarang) {
        this.fPengarang = fPengarang;
        iPengarang = new daoPengarang();
        lPengarang = iPengarang.getAll();
    }
    
    // reset field
    public void reset() {
        fPengarang.getIDPengarang().setText("");
        fPengarang.getNamaPengarang().setText("");
        fPengarang.getAlamatPengarang().setText("");
    }
    
    // menampilkan data ke dalam tabel
    public void isiTabel() {
        lPengarang = iPengarang.getAll();
        tableModelPengarang tmPengarang = new tableModelPengarang(lPengarang);
        fPengarang.getTabelPengarang().setModel(tmPengarang);
    }
    
    // menampilkan data yang dipilih dari tabel
    public void isiField(int row) {
        fPengarang.getIDPengarang().setText(lPengarang.get(row).getIdPengarang().toString());
        fPengarang.getNamaPengarang().setText(lPengarang.get(row).getNamaPengarang());
        fPengarang.getAlamatPengarang().setText(lPengarang.get(row).getAlamat());
        
    }
    
    // insert data 
    public void insert() {
        modelPengarang b = new modelPengarang();
        b.setNamaPengarang(fPengarang.getNamaPengarang().getText()); 
        b.setAlamat(fPengarang.getAlamatPengarang().getText());
        
        iPengarang.insert(b);
    }
    
    //Update data
    public void update() {
        modelPengarang b = new modelPengarang();
        b.setNamaPengarang(fPengarang.getNamaPengarang().getText());
        b.setAlamat(fPengarang.getAlamatPengarang().getText());
        b.setIdPengarang(Integer.parseInt(fPengarang.getIDPengarang().getText()));
        
        iPengarang.update(b);
    }
    
    //delete data 
    public void delete() {
        if(!fPengarang.getIDPengarang().getText().trim().isEmpty()) {
            int id = Integer.parseInt(fPengarang.getIDPengarang().getText());
            iPengarang.delete(id);
        } else {
            JOptionPane.showMessageDialog(fPengarang, "Silahkan Hapus");
        }
    }
}
