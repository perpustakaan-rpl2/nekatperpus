/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.controller;

import admin.dao.daoRak;
import admin.dao.implementRak;
import admin.model.modelRak;
import admin.model.tableModelRak;
import admin.view.frameRak;
import java.util.List;
import javax.swing.JOptionPane;
/**
 *
 * @author Ibang
 */
public class controllerRak {

    frameRak fRak;
    implementRak iRak;
    List<modelRak> lRak;
    
    public controllerRak(frameRak fRak) {
        this.fRak = fRak;
        iRak = new daoRak();
        lRak = iRak.getAll();
    }
    
    // reset field
    public void reset() {
        fRak.getIDRak().setText("");
        fRak.getKategoriRak().setText("");
        fRak.getLokasi().setText("");
    }
    
    // menampilkan data ke dalam tabel
    public void isiTabel() {
        lRak = iRak.getAll();
        tableModelRak tmRak = new tableModelRak(lRak);
        fRak.getTabelRak().setModel(tmRak);
    }
    
    // menampilkan data yang dipilih dari tabel
    public void isiField(int row) {
        fRak.getIDRak().setText(lRak.get(row).getId_rak().toString());
        fRak.getKategoriRak().setText(lRak.get(row).getKategori_rak());
        fRak.getLokasi().setText(lRak.get(row).getLokasi());
        
    }
    
    // insert data 
    public void insert() {
        modelRak b = new modelRak();
        b.setKategori_rak(fRak.getKategoriRak().getText()); 
        b.setLokasi(fRak.getLokasi().getText());
        
        iRak.insert(b);
    }
    
    // update data
    public void update() {
        modelRak b = new modelRak();
        b.setKategori_rak(fRak.getKategoriRak().getText());
        b.setLokasi(fRak.getLokasi().getText());
        b.setId_rak(Integer.parseInt(fRak.getIDRak().getText()));
        
        iRak.update(b);
    }
    
    // delete data 
    public void delete() {
        if(!fRak.getIDRak().getText().trim().isEmpty()) {
            int id = Integer.parseInt(fRak.getIDRak().getText());
            iRak.delete(id);
        } else {
            JOptionPane.showMessageDialog(fRak, "Pilih data yang akan di hapus!");
        }
    }
    
}
