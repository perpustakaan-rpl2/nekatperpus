/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.model;

/**
 *
 * @author dalex
 */
public class modelPengarang {
    
    private Integer id_pengarang;
    private String nama_pengarang;
    private String alamat;
    
    public Integer getIdPengarang()
    {
        return id_pengarang;
    }
    
    public void setIdPengarang(Integer id_pengarang)
    {
        this.id_pengarang = id_pengarang;
    }
    
    public String getNamaPengarang()
    {
        return nama_pengarang;
    }
    
    public void setNamaPengarang(String nama_pengarang)
    {
        this.nama_pengarang = nama_pengarang;
    }
    
    public String getAlamat()
    {
        return alamat;
    }
    
    public void setAlamat(String alamat)
    {
        this.alamat = alamat;
    }
    
}
