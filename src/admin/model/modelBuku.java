/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.model;

/**
 *
 * @author User
 */
public class modelBuku {
    
    private Integer id_buku;
    private Integer id_penerbit;
    private Integer id_pengarang;
    private Integer id_rak;
    private String judul_buku;
    private String tahun_terbit;
    private String jumlah_buku;
    private String tanggal_pengadaan;
    private String nama_penerbit;
    private String nama_pengarang;
    private String kategori_rak;

    public Integer getId_buku() {
        return id_buku;
    }

    public void setId_buku(Integer id_buku) {
        this.id_buku = id_buku;
    }

    public Integer getId_penerbit() {
        return id_penerbit;
    }

    public void setId_penerbit(Integer id_penerbit) {
        this.id_penerbit = id_penerbit;
    }

    public Integer getId_pengarang() {
        return id_pengarang;
    }

    public void setId_pengarang(Integer id_pengarang) {
        this.id_pengarang = id_pengarang;
    }

    public Integer getId_rak() {
        return id_rak;
    }

    public void setId_rak(Integer id_rak) {
        this.id_rak = id_rak;
    }
    

    public String getJudul_buku() {
        return judul_buku;
    }

    public void setJudul_buku(String judul_buku) {
        this.judul_buku = judul_buku;
    }

    public String getTahun_terbit() {
        return tahun_terbit;
    }

    public void setTahun_terbit(String tahun_terbit) {
        this.tahun_terbit = tahun_terbit;
    }

    public String getJumlah_buku() {
        return jumlah_buku;
    }

    public void setJumlah_buku(String jumlah_buku) {
        this.jumlah_buku = jumlah_buku;
    }

    public String getTanggal_pengadaan() {
        return tanggal_pengadaan;
    }

    public void setTanggal_pengadaan(String tanggal_pengadaan) {
        this.tanggal_pengadaan = tanggal_pengadaan;
    }

    public String getNama_penerbit() {
        return nama_penerbit;
    }

    public void setNama_penerbit(String nama_penerbit) {
        this.nama_penerbit = nama_penerbit;
    }

    public String getNama_pengarang() {
        return nama_pengarang;
    }

    public void setNama_pengarang(String nama_pengarang) {
        this.nama_pengarang = nama_pengarang;
    }

    public String getKategori_rak() {
        return kategori_rak;
    }

    public void setKategori_rak(String kategori_rak) {
        this.kategori_rak = kategori_rak;
    }
    
    
    
}
