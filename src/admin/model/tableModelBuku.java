/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.model;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author User
 */
public class tableModelBuku extends AbstractTableModel{
     List<modelBuku> listBuk;
    
    public tableModelBuku(List<modelBuku> listBuk) {
        this.listBuk = listBuk;
    }

    @Override
    public int getRowCount() {
        return listBuk.size();
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public Object getValueAt(int row, int col) {
        switch(col) {
            case 0:
                return listBuk.get(row).getJudul_buku();
            case 1:
                return listBuk.get(row).getNama_penerbit();
            case 2:
                return listBuk.get(row).getNama_pengarang();
            case 3:
                return listBuk.get(row).getKategori_rak();
            case 4:
                return listBuk.get(row).getTahun_terbit();
            case 5:
                return listBuk.get(row).getJumlah_buku();
            case 6:
                return listBuk.get(row).getTanggal_pengadaan();
            default:
                return null;
        } 
    }
    
}
