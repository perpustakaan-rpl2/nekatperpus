/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.model;

import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author dalex
 */
public class tableModelPengarang extends AbstractTableModel 
{    
    List<modelPengarang> listPeng;
    
    public tableModelPengarang(List<modelPengarang> listPeng) {
        this.listPeng = listPeng;
    }

    @Override
    public int getRowCount() {
        return listPeng.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int row, int col) {
        switch(col) {
            case 0:
                return listPeng.get(row).getIdPengarang();
            case 1:
                return listPeng.get(row).getNamaPengarang();
            case 2:
                return listPeng.get(row).getAlamat();
            default:
                return null;
        } 
    }
    
}
