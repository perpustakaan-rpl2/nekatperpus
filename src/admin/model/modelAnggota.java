/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.model;

/**
 *
 * @author Risma Septiani
 */
public class modelAnggota {
    private Integer id_anggota;
    private String nama_anggota;
    private String alamat;
    private String jurusan;
    private String no_telp;
    
    public Integer getIdAnggota()
    {
        return id_anggota;
    }
    
    public void setIdAnggota(Integer id_anggota)
    {
        this.id_anggota = id_anggota;
    }
    
    public String getNamaAnggota()
    {
        return nama_anggota;
    }
    
    public void setNamaAnggota(String nama_anggota)
    {
        this.nama_anggota = nama_anggota;
    }
    
    public String getJurusan ()
    {
        return jurusan;
    }
    
    public void setJurusan (String jurusan)
    {
        this.jurusan = jurusan;
    }
    
    public String getAlamat()
    {
        return alamat;
    }
    
    public void setAlamat(String alamat)
    {
        this.alamat = alamat;
    }
    
    public String getNoTelp()
    {
        return no_telp;
    }
    
    public void setNoTelp(String no_telp)
    {
        this.no_telp = no_telp;
    }
    

    public void setAnggota(String text) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
