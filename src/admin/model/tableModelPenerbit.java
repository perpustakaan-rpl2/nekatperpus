/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.model;

import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Ibang
 */
public class tableModelPenerbit extends AbstractTableModel 
{    
    List<modelPenerbit> listPen;
    
    public tableModelPenerbit(List<modelPenerbit> listPen) {
        this.listPen = listPen;
    }

    @Override
    public int getRowCount() {
        return listPen.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int row, int col) {
        switch(col) {
            case 0:
                return listPen.get(row).getIdPenerbit();
            case 1:
                return listPen.get(row).getNamaPenerbit();
            case 2:
                return listPen.get(row).getAlamat();
            default:
                return null;
        } 
    }
    
}
