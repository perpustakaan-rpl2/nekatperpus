/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.model;

import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Risma Septiani
 */
public class tableModelAnggota extends AbstractTableModel {
    List<modelAnggota> listAngg;
    public tableModelAnggota(List<modelAnggota> listAngg) {
        this.listAngg = listAngg;
    }
    
    @Override
    public int getRowCount() {
        return listAngg.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Object getValueAt(int row, int col) {
        switch(col) {
            case 0:
                return listAngg.get(row).getIdAnggota();
            case 1:
                return listAngg.get(row).getNamaAnggota();
            case 2:
                return listAngg.get (row).getJurusan();
            case 3:
                return listAngg.get(row).getAlamat();
            case 4:
                return listAngg.get(row).getNoTelp();
            default:
                return null;
        } 
    }
    
}
