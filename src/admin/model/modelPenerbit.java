/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.model;

/**
 *
 * @author Ibang
 */
public class modelPenerbit {
    
    private Integer id_penerbit;
    private String nama_penerbit;
    private String alamat;
    
    public Integer getIdPenerbit()
    {
        return id_penerbit;
    }
    
    public void setIdPenerbit(Integer id_penerbit)
    {
        this.id_penerbit = id_penerbit;
    }
    
    public String getNamaPenerbit()
    {
        return nama_penerbit;
    }
    
    public void setNamaPenerbit(String nama_penerbit)
    {
        this.nama_penerbit = nama_penerbit;
    }
    
    public String getAlamat()
    {
        return alamat;
    }
    
    public void setAlamat(String alamat)
    {
        this.alamat = alamat;
    }
    
}
