/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.model;

/**
 *
 * @author Ibang
 */
public class modelRak {
    
    private Integer id_rak;
    private String kategori_rak;
    private Integer jumlah_buku;
    private String lokasi;

    public Integer getId_rak() {
        return id_rak;
    }

    public void setId_rak(Integer id_rak) {
        this.id_rak = id_rak;
    }

    public String getKategori_rak() {
        return kategori_rak;
    }

    public void setKategori_rak(String kategori_rak) {
        this.kategori_rak = kategori_rak;
    }

    public Integer getJumlah_buku() {
        return jumlah_buku;
    }

    public void setJumlah_buku(Integer jumlah_buku) {
        this.jumlah_buku = jumlah_buku;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }
    
}
