/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.model;

import java.util.List;
import javax.swing.table.AbstractTableModel;
/**
 *
 * @author Ibang
 */
public class tableModelRak extends AbstractTableModel {
    List<modelRak> listRak;
    
    public tableModelRak(List<modelRak> listRak) {
        this.listRak = listRak;
    }

    @Override
    public int getRowCount() {
        return listRak.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int row, int col) {
        switch(col) {
            case 0:
                return listRak.get(row).getId_rak();
            case 1:
                return listRak.get(row).getKategori_rak();
            case 2:
                return listRak.get(row).getJumlah_buku();
            case 3:
                return listRak.get(row).getLokasi();
            default:
                return null;
        } 
    }
}
