/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.dao;

import admin.koneksi.koneksi;
import admin.model.modelPengarang;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dalex
 */
public class daoPengarang implements implementPengarang {
    
    Connection con;
    final String insert = "INSERT INTO pengarang (nama_pengarang, alamat) VALUES (?, ?);";
    final String update = "UPDATE pengarang SET nama_pengarang=?, alamat=? where id_pengarang=?;";
    final String delete = "DELETE FROM pengarang where id_pengarang=?;";
    final String select = "SELECT * FROM pengarang;";
    final String carinama = "SELECT * FROM pengarang where nama_pengarang like ?;";
    
    public daoPengarang() {
        con = koneksi.connection();
    }

    public void insert(modelPengarang b) {
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(insert);
            st.setString(1, b.getNamaPengarang());
            st.setString(2, b.getAlamat());
            st.executeUpdate();
//            ResultSet rs = st.getGeneratedKeys();
//            while(rs.next()) {
//                b.setIdPenerbit(rs.getInt(1));
//            }
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                st.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void update(modelPengarang b) {
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(update);
            st.setString(1, b.getNamaPengarang());
            st.setString(2, b.getAlamat());
            st.setInt(3, b.getIdPengarang());
            st.executeUpdate();
            
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                st.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void delete(int id) {
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(delete);
            
            st.setInt(1, id);
            st.executeUpdate();
            
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                st.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public List<modelPengarang> getAll() {
        List<modelPengarang> listPeng = null;
        try {
            listPeng = new ArrayList<modelPengarang>();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(select);
            while(rs.next()) {
                modelPengarang b = new modelPengarang();
                b.setIdPengarang(rs.getInt("id_pengarang"));
                b.setNamaPengarang(rs.getString("nama_pengarang"));
                b.setAlamat(rs.getString("alamat"));
                listPeng.add(b);
            }
        } catch(SQLException ex) {
            Logger.getLogger(daoPengarang.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return listPeng;
    }

    public List<modelPengarang> getCariNama(String nama) {
        List<modelPengarang> listPeng = null;
        try {
            listPeng = new ArrayList<modelPengarang>();
            PreparedStatement st = con.prepareStatement(carinama);
            st.setString(1, "%" + nama + "%");
            ResultSet rs = st.executeQuery(carinama);
            while(rs.next()) {
                modelPengarang b = new modelPengarang();
                b.setIdPengarang(rs.getInt("id"));
                b.setNamaPengarang(rs.getString("nama"));
                b.setAlamat(rs.getString("alamat"));
                listPeng.add(b);
            }
        } catch(SQLException ex) {
            Logger.getLogger(daoPengarang.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return listPeng;
    }
    
}
