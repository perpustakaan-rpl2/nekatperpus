/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.dao;

import admin.koneksi.koneksi;
import admin.model.modelRak;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Ibang
 */
public class daoRak implements implementRak{
    
    Connection con;
    final String insert = "INSERT INTO rak (kategori_rak, lokasi) VALUES (?, ?);";
    final String update = "UPDATE rak SET kategori_rak=?, lokasi=? where id_rak=?;";
    final String delete = "DELETE FROM rak where id_rak=?;";
    final String select = "SELECT * FROM rak;";
//    final String carinama = "SELECT * FROM penerbit where nama_penerbit like ?;";
    
    public daoRak() {
        con = koneksi.connection();
    }

    @Override
    public void insert(modelRak b) {
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(insert);
            st.setString(1, b.getKategori_rak());
            st.setString(2, b.getLokasi());
            st.executeUpdate();
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                st.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void update(modelRak b) {
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(update);
            st.setString(1, b.getKategori_rak());
            st.setString(2, b.getLokasi());
            st.setInt(3, b.getId_rak());
            st.executeUpdate();
            
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                st.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void delete(int id) {
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(delete);
            
            st.setInt(1, id);
            st.executeUpdate();
            
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                st.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public List<modelRak> getAll() {
        List<modelRak> listRak = null;
        try {
            listRak = new ArrayList<modelRak>();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(select);
            while(rs.next()) {
                modelRak b = new modelRak();
                b.setId_rak(rs.getInt("id_rak"));
                b.setKategori_rak(rs.getString("kategori_rak"));
                b.setLokasi(rs.getString("lokasi"));
                listRak.add(b);
            }
        } catch(SQLException ex) {
            Logger.getLogger(daoPenerbit.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return listRak;
    }
    
}
