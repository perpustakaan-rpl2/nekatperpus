/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.dao;

import admin.koneksi.koneksi;
import admin.model.modelAnggota;
import admin.model.modelKelas;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Risma Septiani
 */
public class daoKelas implements implementKelas{

    Connection con;
    final String select = "SELECT * FROM kelas;";
    
    public daoKelas() {
        con = koneksi.connection();
    }
    @Override
    public List<modelKelas> getAll() {
        List<modelKelas> listKel = null;
        try {
            listKel = new ArrayList<modelKelas>();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(select);
            while(rs.next()) {
                modelKelas b = new modelKelas();
                b.setId_kelas(rs.getInt("id_kelas"));
                b.setNama_kelas(rs.getString("nama_kelas"));
                listKel.add(b);
            }
        } catch(SQLException ex) {
            Logger.getLogger(daoKelas.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return listKel;
        
    }
    
    
}
