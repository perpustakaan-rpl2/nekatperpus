/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.dao;

import java.util.List;
import admin.model.*;
/**
 *
 * @author Ibang
 */
public interface implementRak {
    
    public void insert(modelRak b);
    
    public void update(modelRak b);
    
    public void delete(int id);
    
    public List<modelRak> getAll();
    
}
