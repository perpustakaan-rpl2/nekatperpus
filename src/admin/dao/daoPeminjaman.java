/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.dao;

import admin.koneksi.koneksi;
import admin.model.modelPeminjaman;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ibang
 */
public class daoPeminjaman implements implementPeminjaman {
    Connection con;
    
    final String insert = "INSERT INTO peminjaman (id_anggota, id_buku, tanggal_pinjam, tanggal_kembali) VALUES (?, ?, ?, ?);";
    final String select = "SELECT * FROM peminjaman;";
    
    public daoPeminjaman() {
        con = koneksi.connection();
    }

    public void insert(modelPeminjaman b) {
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(insert);
            st.setInt(1, b.getId_anggota());
            st.setInt(2, b.getId_buku());
            st.setString(3, b.getTanggal_pinjam());
            st.setString(4, b.getTanggal_kembali());
            st.executeUpdate();
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                st.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public List<modelPeminjaman> getAll() {
        List<modelPeminjaman> listPem = null;
        try {
            listPem = new ArrayList<modelPeminjaman>();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(select);
            while(rs.next()) {
                modelPeminjaman b = new modelPeminjaman();
                b.setId_peminjaman(rs.getInt("id_peminjaman"));
                b.setId_anggota(rs.getInt("id_anggota"));
                b.setId_buku(rs.getInt("id_buku"));
                b.setTanggal_pinjam(rs.getString("tanggal_pinjam"));
                b.setTanggal_kembali(rs.getString("tanggal_kembali"));
                b.setDenda(rs.getInt("denda"));
                listPem.add(b);
            }
        } catch(SQLException ex) {
            Logger.getLogger(daoAnggota.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return listPem;
    }
    
}
