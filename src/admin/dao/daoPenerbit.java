/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.dao;

import admin.koneksi.koneksi;
import admin.model.modelPenerbit;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ibang
 */
public class daoPenerbit implements implementPenerbit {
    
    Connection con;
    final String insert = "INSERT INTO penerbit (nama_penerbit, alamat) VALUES (?, ?);";
    final String update = "UPDATE penerbit SET nama_penerbit=?, alamat=? where id_penerbit=?;";
    final String delete = "DELETE FROM penerbit where id_penerbit=?;";
    final String select = "SELECT * FROM penerbit;";
    final String carinama = "SELECT * FROM penerbit where nama_penerbit like ?;";
    
    public daoPenerbit() {
        con = koneksi.connection();
    }

    public void insert(modelPenerbit b) {
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(insert);
            st.setString(1, b.getNamaPenerbit());
            st.setString(2, b.getAlamat());
            st.executeUpdate();
//            ResultSet rs = st.getGeneratedKeys();
//            while(rs.next()) {
//                b.setIdPenerbit(rs.getInt(1));
//            }
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                st.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void update(modelPenerbit b) {
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(update);
            st.setString(1, b.getNamaPenerbit());
            st.setString(2, b.getAlamat());
            st.setInt(3, b.getIdPenerbit());
            st.executeUpdate();
            
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                st.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void delete(int id) {
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(delete);
            
            st.setInt(1, id);
            st.executeUpdate();
            
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                st.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public List<modelPenerbit> getAll() {
        List<modelPenerbit> listPen = null;
        try {
            listPen = new ArrayList<modelPenerbit>();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(select);
            while(rs.next()) {
                modelPenerbit b = new modelPenerbit();
                b.setIdPenerbit(rs.getInt("id_penerbit"));
                b.setNamaPenerbit(rs.getString("nama_penerbit"));
                b.setAlamat(rs.getString("alamat"));
                listPen.add(b);
            }
        } catch(SQLException ex) {
            Logger.getLogger(daoPenerbit.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return listPen;
    }

    public List<modelPenerbit> getCariNama(String nama) {
        List<modelPenerbit> listPen = null;
        try {
            listPen = new ArrayList<modelPenerbit>();
            PreparedStatement st = con.prepareStatement(carinama);
            st.setString(1, "%" + nama + "%");
            ResultSet rs = st.executeQuery(carinama);
            while(rs.next()) {
                modelPenerbit b = new modelPenerbit();
                b.setIdPenerbit(rs.getInt("id"));
                b.setNamaPenerbit(rs.getString("nama"));
                b.setAlamat(rs.getString("alamat"));
                listPen.add(b);
            }
        } catch(SQLException ex) {
            Logger.getLogger(daoPenerbit.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return listPen;
    }
    
}
