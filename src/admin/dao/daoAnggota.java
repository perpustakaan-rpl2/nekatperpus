/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.dao;
import admin.koneksi.koneksi;
import admin.model.modelAnggota;
import admin.model.modelRak;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Risma Septiani
 */
public class daoAnggota implements implementAnggota {
    Connection con;
    final String update = "UPDATE anggota SET nama_anggota=?, jurusan=? , alamat=? , no_telp=? where id_anggota=?;";
    final String insert = "INSERT INTO anggota (nama_anggota,jurusan, alamat, no_telp) VALUES (?, ?, ?, ?);";
    final String delete = "DELETE FROM anggota where id_anggota=?;";
    final String select = "SELECT * FROM anggota;";
    
    public daoAnggota() {
        con = koneksi.connection();
    }

    public void insert(modelAnggota b) {
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(insert);
            st.setString(1, b.getNamaAnggota());
            st.setString(2, b.getJurusan());
            st.setString(3, b.getAlamat());
            st.setString(4, b.getNoTelp());
            st.executeUpdate();
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                st.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    @Override
    public void update(modelAnggota b) {
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(update);
            st.setString(1, b.getNamaAnggota());
            st.setString(2, b.getJurusan());
            st.setString(3, b.getAlamat());
            st.setString(4, b.getNoTelp());
            st.setInt(5, b.getIdAnggota());
            st.executeUpdate();
            
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                st.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public void delete(int id) {
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(delete);
            
            st.setInt(1, id);
            st.executeUpdate();
            
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                st.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public List<modelAnggota> getAll() {
        List<modelAnggota> listAngg = null;
        try {
            listAngg = new ArrayList<modelAnggota>();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(select);
            while(rs.next()) {
                modelAnggota b = new modelAnggota();
                b.setIdAnggota(rs.getInt("id_anggota"));
                b.setNamaAnggota(rs.getString("nama_anggota"));
                b.setJurusan(rs.getString("jurusan"));
                b.setAlamat(rs.getString("alamat"));
                b.setNoTelp(rs.getString("no_telp"));
                listAngg.add(b);
            }
        } catch(SQLException ex) {
            Logger.getLogger(daoAnggota.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return listAngg;
    }
    
}
