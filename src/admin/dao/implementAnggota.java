/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.dao;

import java.util.List;
import admin.model.*;

/**
 *
 * @author Risma Septiani
 */
public interface implementAnggota {
    public void insert(modelAnggota b);
    
    public void delete(int id);
    
    public void update(modelAnggota b);

    public List<modelAnggota> getAll();
    
}
