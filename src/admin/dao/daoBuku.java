/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.dao;

import admin.koneksi.koneksi;
import admin.model.modelBuku;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class daoBuku implements implementBuku{
    Connection con;
    final String insert = "INSERT INTO buku (judul_buku, id_penerbit, id_pengarang, id_rak, tahun_terbit, jumlah_buku, tanggal_pengadaan) VALUES (?, ?, ?, ?, ?, ?, ?);";
    final String update = "UPDATE buku SET judul_buku=?, id_penerbit=?, id_pengarang=?, id_rak=?, tahun_terbit=?, jumlah_buku=?, tanggal_pengadaan=? where id_buku=?;";
    final String delete = "DELETE FROM buku where id_buku=?;";
    final String select = "SELECT * FROM buku JOIN penerbit ON buku.id_penerbit=penerbit.id_penerbit JOIN pengarang ON buku.id_pengarang=pengarang.id_pengarang JOIN rak ON buku.id_rak=rak.id_rak;";
    
    public daoBuku() {
        con = koneksi.connection();
    }

    @Override
    public void insert(modelBuku b) {
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(insert);
            st.setString(1, b.getJudul_buku());
            st.setInt(2, b.getId_penerbit());
            st.setInt(3, b.getId_pengarang());
            st.setInt(4, b.getId_rak());
            st.setString(5, b.getTahun_terbit());
            st.setString(6, b.getJumlah_buku());
            st.setString(7, b.getTanggal_pengadaan());
            st.executeUpdate();
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                st.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
@Override
    public void update(modelBuku b) {
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(update);
            st.setString(1, b.getJudul_buku());
            st.setInt(2, b.getId_penerbit());
            st.setInt(3, b.getId_pengarang());
            st.setInt(4, b.getId_rak());
            st.setString(5, b.getTahun_terbit());
            st.setString(6, b.getJumlah_buku());
            st.setString(7, b.getTanggal_pengadaan());
            st.setInt(8, b.getId_buku());
            st.executeUpdate();
            
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                st.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
@Override
    public void delete(int id) {
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(delete);
            
            st.setInt(1, id);
            st.executeUpdate();
            
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                st.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
@Override
    public List<modelBuku> getAll() {
        List<modelBuku> listBuk = null;
        try {
            listBuk = new ArrayList<modelBuku>();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(select);
            while(rs.next()) {
                modelBuku b = new modelBuku();
                b.setId_buku(rs.getInt("id_buku"));
                b.setId_penerbit(rs.getInt("id_penerbit"));
                b.setId_pengarang(rs.getInt("id_pengarang"));
                b.setId_rak(rs.getInt("id_rak"));
                b.setJudul_buku(rs.getString("judul_buku"));
                b.setTahun_terbit(rs.getString("tahun_terbit"));
                b.setJumlah_buku(rs.getString("jumlah_buku"));
                b.setTanggal_pengadaan(rs.getString("tanggal_pengadaan"));
                b.setNama_penerbit(rs.getString("nama_penerbit"));
                b.setNama_pengarang(rs.getString("nama_pengarang"));
                b.setKategori_rak(rs.getString("kategori_rak"));
                listBuk.add(b);
            }
        } catch(SQLException ex) {
            Logger.getLogger(daoBuku.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return listBuk;
    }
    
}
