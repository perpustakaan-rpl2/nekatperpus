/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.dao;

import java.util.List;
import admin.model.*;

/**
 *
 * @author dalex
 */
public interface implementPengarang {
    
    public void insert(modelPengarang b);
    
    public void update(modelPengarang b);
    
    public void delete(int id);
    
    public List<modelPengarang> getAll();
    
    public List<modelPengarang> getCariNama(String nama);
    
}
