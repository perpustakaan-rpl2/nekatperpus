/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.dao;

import java.util.List;
import admin.model.*;

/**
 *
 * @author Ibang
 */
public interface implementPenerbit {
    
    public void insert(modelPenerbit b);
    
    public void update(modelPenerbit b);
    
    public void delete(int id);
    
    public List<modelPenerbit> getAll();
    
    public List<modelPenerbit> getCariNama(String nama);
    
}
