/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.dao;

import java.util.List;
import admin.model.*;

/**
 *
 * @author User
 */
public interface implementBuku {
    public void insert(modelBuku b);
    
    public void update(modelBuku b);
    
    public void delete(int id);
    
    public List<modelBuku> getAll();
    
}
